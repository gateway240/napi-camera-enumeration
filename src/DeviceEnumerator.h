#ifndef UNICODE
#define UNICODE
#endif

#include <Windows.h>
#include <dshow.h>

#include <napi.h>
#include <node_api.h>
#include <iostream>
#include <stdio.h>
#include <comdef.h>
#include <string>
#include <vector>

#pragma comment(lib, "strmiids")

struct Device {
	int id; // This can be used to open the device in OpenCV
	std::string devicePath;
	std::string deviceName; // This can be used to show the devices to the user
};

class DeviceEnumerator {

public:
	static std::vector<Device> getDevices(const GUID deviceClass);

private:

	static std::string ConvertBSTRToMBS(BSTR bstr);
	static std::string ConvertWCSToMBS(const wchar_t* pstr, long wslen);

};

namespace Interface {

Napi::Array devicesToNapiArray(const Napi::CallbackInfo &info, const GUID deviceClass);

Napi::Array getVideoDevices(const Napi::CallbackInfo &info);
Napi::Array getAudioDevices(const Napi::CallbackInfo &info);


 //Export API
Napi::Object Init(Napi::Env env, Napi::Object exports);
NODE_API_MODULE(addon, Init)
}